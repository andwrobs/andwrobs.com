import { defineMDSveXConfig as defineConfig } from 'mdsvex';

const config = defineConfig({
	extensions: ['.svelte.md'],

	smartypants: {
		dashes: 'oldschool'
	}
});

export default config;
