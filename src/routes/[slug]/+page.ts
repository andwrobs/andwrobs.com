import { fetch_posts } from '$lib/assets/js/fetch_posts.js';
import { error } from '@sveltejs/kit';

export const prerender = true;

export const load = async ({ params }: { params: { slug: string } }) => {
	try {
		const post = await import(`../../lib/assets/posts/${params.slug}.svelte.md`);
		const PostContent = post.default;

		return {
			PostContent,
			meta: { ...post.metadata, slug: params.slug }
		};
	} catch (err) {
		throw error(404);
	}
};

export async function entries() {
	try {
		const { posts } = await fetch_posts();
		return posts.map((post) => ({ slug: post.slug }));
	} catch (err) {}
}
