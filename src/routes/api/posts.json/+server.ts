import { fetch_posts } from '$lib/assets/js/fetch_posts';
import { json } from '@sveltejs/kit';

export const prerender = true;

export const GET = async ({ params }) => {
	const { posts } = await fetch_posts();
	return json(posts);
};
