---
title: Serverless vs Containers
categories: notes
---

# Databases

# Cloud providers
## Serverless vs Containers

When deciding between serverless and containers for hosting applications, the choice largely depends on the specific use case and requirements of the application. Here's a general comparison:

 Serverless | Containers |
---|---|---|
**Use Case**| Internet-facing applications | Internal tools for small-medium sized businesses |
**Cost**| Pay only for the compute time you consume. There is no charge when your code is not running. | Infrastructure costs can be around ~$300 a month, which may not be prohibitive for certain businesses. |
**Scalability**| Automatically scales with the size of the workload. | Manual configuration needed for scaling. |
**Management**| No need to provision or manage servers. | Need to manage the orchestration and health of the system. |

In summary, if you're building an application that is internet-facing, serverless could be a more cost-effective and scalable solution. On the other hand, if you're building internal tools for a small to medium-sized business, where infrastructure costs of around $300 a month aren't prohibitive, container orchestration could be a great choice.
