/**
 * theme
 */

export type SiteColor = 'red' | 'yellow' | 'green' | 'blue' | 'violet';

export const SiteBgColor: Record<SiteColor, string> = {
	blue: 'bg-blue-600/10',
	green: 'bg-green-600/10',
	red: 'bg-red-600/10',
	violet: 'bg-violet-600/10',
	yellow: 'bg-yellow-600/10'
};
